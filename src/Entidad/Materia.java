/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

import ufps.util.colecciones_seed.Pila;

/**
 *
 * @author Boris M
 */
public class Materia {

    private byte codMateria;
    private String nombre;
    Pila<Ensayo> ensayos = new Pila<>();

    public Materia() {
    }

    public boolean perteneceMateria(int cod) {

        return cod == this.getCodMateria();

    }

 
    public int calcularNota(Estudiante estu){
    
        Pila<Ensayo> aux = new Pila<>();
        int cant=0;
        int nota=0;
        while(!ensayos.esVacia()){
         Ensayo ensy=ensayos.desapilar(); 
            if(ensy.getEscritor().equals(estu))
            {
            cant++;
            nota+=ensy.sacarNota();
            aux.apilar(ensy);
            }
            else 
                aux.apilar(ensy);
        }
        vaciar(aux);
        return nota/cant;
    }
    
    private void vaciar(Pila<Ensayo> ens){
    while(!ens.esVacia())
        ensayos.apilar(ens.desapilar());
    }
    
    //busca al estudiante en la pila de ensayos
    public boolean pertenece(Estudiante es){
    
           Pila<Ensayo> aux = new Pila<>();
        while (!ensayos.esVacia()) {
        Ensayo ensy=ensayos.desapilar();
        
        if(ensy.getEscritor().equals(es)){
        aux.apilar(ensy);
            vaciar(aux);
            return true;
        }
        else aux.apilar(ensy);
        } 
        vaciar(aux);
        return false;
    }
       
    
    public void agregarEnsayo(Ensayo y) {
        ensayos.apilar(y);
    }

    public Materia(byte codMateria, String nombre) {
        this.codMateria = codMateria;
        this.nombre = nombre;
    }

    public byte getCodMateria() {
        return codMateria;
    }

    public void setCodMateria(byte codMateria) {
        this.codMateria = codMateria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Pila<Ensayo> getEnsayos() {
        return ensayos;
    }

    public String listaEnsayos() {
        String ms = "";
       Pila<Ensayo> aux=new Pila<>();
        
        
        while (!ensayos.esVacia()) {
        Ensayo dato=ensayos.desapilar();   
            ms += dato + "\n";
        aux.apilar(dato);
        }
        vaciar(aux);
        return ms;
    }

    @Override
    public String toString() {
        return "Materia{" + "codMateria=" + codMateria + ", nombre=" + nombre + "Todos os ensayos " + listaEnsayos() + '}';
    }

}
