/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Entidad.Ensayo;
import Entidad.Estudiante;
import Entidad.Materia;
import ufps.util.colecciones_seed.Cola;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author Boris M
 */
public class SistemaCalificacion {
   
    Cola<Materia> materias=new Cola();
    ListaCD<Estudiante> estudiantes=new ListaCD<>();
    
    public  SistemaCalificacion(String urlEStudiante,String urlMaterias,String urlEnsayos)
    {
    crearEstudiantes(urlEStudiante);
    crearMaterias(urlMaterias);
    this.crearEnsayo(urlEnsayos);
    }
    
    
    public String ListaMaterias(){
    
        String ms="";
    Cola<Materia> aux=new Cola<>();
         
    while(!materias.esVacia()){
   
        Materia dato=materias.deColar();    
        ms+=dato+"\n";
        aux.enColar(dato);
        
    }
   vaciar(aux);
           return ms;
    }
    
    private void vaciar(Cola<Materia> col){
    while(!col.esVacia())
        materias.enColar(col.deColar());
    }
    
    private void crearEstudiantes(String url){
        
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object v[] = archivo.leerArchivo();
        
        for(int i=1;i<v.length;i++){
        String linea = v[i].toString();
        String datos[] = linea.split(";");
            
        int codigo=Integer.parseInt(datos[0]);
        String nombre=datos[1];
        boolean genero=Boolean.parseBoolean(datos[2]);
        
        Estudiante est=new Estudiante(codigo, nombre, genero);
        estudiantes.insertarAlFinal(est);
        
        }
        
    }
    
    private void crearMaterias(String url){
    
     ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object v[] = archivo.leerArchivo();
        
        for(int i=1;i<v.length;i++){
        String linea = v[i].toString();
        String datos[] = linea.split(";");
        
        byte codigoMat=Byte.parseByte(datos[0]);
        String nombre=datos[1];
        Materia nueva=new Materia(codigoMat, nombre);
        this.materias.enColar(nueva);
        
        
    }
        
    }
    
  
    private void crearEnsayo(String url)
    {
    
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object v[] = archivo.leerArchivo();
        
        for(int i=1;i<v.length;i++){
        String linea = v[i].toString();
        String datos[] = linea.split(";");
        
        int codigoMat=Integer.parseInt(datos[0]);
        int codigoEst=Integer.parseInt(datos[1]);
        String cuerpoTra=datos[2];
        int cantHojas=Integer.parseInt(datos[3]);
        
        Estudiante est=buscarDueño(codigoEst);
        
        Ensayo ensa=new Ensayo(cuerpoTra, cantHojas,est);;
        if(est!=null){
        ensa.setEscritor(est);
      //Ensayo ensa=new Ensayo(cuerpoTra, cantHojas,);
        }
  
        Materia m=buscarMateria(codigoMat); 
        if(m!=null)
        m.agregarEnsayo(ensa);
    
        }
        
    }
    
    public String calificaciones(){
    String msg="";
        
    for(Estudiante est:estudiantes){
    msg+="El Estudiante"+est.getNombre()+"   "+"Tiene Las Materias"+"\n"+buscarCursosEstudiante(est)+"\n";
    }
    return msg;
    }
    
    //busca los cursos a los que pertenece el estudiante y le calcula su nota
    private String buscarCursosEstudiante(Estudiante est)
    {
       String ms="";
        Cola<Materia> aux=new Cola<>();
    while(!materias.esVacia())
    {
         
        Materia m=materias.deColar();
    if(m.pertenece(est))
    {
    ms+="Materia:"+m.getNombre()+"   "+"Definitiva:"+"   "+m.calcularNota(est)+"\n";
    aux.enColar(m);
    }
    else 
        aux.enColar(m);        
    }
        vaciar(aux);
        return ms;
    }
    
    private Estudiante buscarDueño(int cod){
    
        for(Estudiante es:estudiantes){
        if(es.getCodigo()==cod)
            return es;
        }
        return null;
    }
    
    private Materia buscarMateria(int cod){
    
        Cola<Materia> aux=new Cola<>();
        
        while(!materias.esVacia()){
            
        Materia dato=materias.deColar();
        
        if(dato.getCodMateria()!=(byte)cod) 
        {
        aux.enColar(dato);
        }
            materias.enColar(dato); 
            return dato;
        }
        
        vaciar(aux);
        return null;
    }
    
  
        
}
