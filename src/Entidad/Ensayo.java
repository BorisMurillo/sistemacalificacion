/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

/**
 *
 * @author Boris M
 */
public class Ensayo {
    
    private String contenidoEnsa;
    private int cantHojas;
    private Estudiante escritor;
    

    public Ensayo() {
    }

    public Ensayo(String contenidoEnsa, int cantHojas, Estudiante escritor) {
        this.contenidoEnsa = contenidoEnsa;
        this.cantHojas = cantHojas;
        this.escritor = escritor;
    }

    public int sacarNota()
    {
        
    if(escritor.isGenero())
    {
   if(cantHojas<=3 && cantHojas>0)
       return 3;
   else if(cantHojas<=5)
       return 4;
   else
       return 5;
    }
    
     if(cantHojas<=3 && cantHojas>0)
       return 2;
   else if(cantHojas<=5)
       return 3;
   else
       return 4;   
    }
    
    public Estudiante getEscritor() {
        return escritor;
    }

    public void setEscritor(Estudiante escritor) {
        this.escritor = escritor;
    }
 
    public Ensayo(String contenidoEnsa, int cantHojas) {
        this.contenidoEnsa = contenidoEnsa;
        this.cantHojas = cantHojas;
    }

    public String getContenidoEnsa() {
        return contenidoEnsa;
    }

    public void setContenidoEnsa(String contenidoEnsa) {
        this.contenidoEnsa = contenidoEnsa;
    }

    public int getCantHojas() {
        return cantHojas;
    }

    public void setCantHojas(int cantHojas) {
        this.cantHojas = cantHojas;
    }

    @Override
    public String toString() {
        return "Ensayo{" + "contenidoEnsa=" + contenidoEnsa + ", cantHojas=" + cantHojas + "escritor del ensayo"+ escritor+'}';
    }
    
    
    
}
