/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

/**
 *
 * @author Boris M
 */
public class Estudiante {
    
    private int codigo;
    private String nombre;
    private boolean genero ;

    public Estudiante() {
    }

    public Estudiante(int codigo, String nombre, boolean genero) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.genero = genero;
    }

  
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isGenero() {
        return genero;
    }

    public void setGenero(boolean genero) {
        this.genero = genero;
    }

    @Override
    public String toString() {
        return "Estudiante{" + "codigo=" + codigo + ", nombre=" + nombre + ", genero=" + genero + '}';
    }
    
   
    
}
